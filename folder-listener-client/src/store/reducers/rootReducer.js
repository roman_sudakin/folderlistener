import authReducer from './authReducer';
import  foldersReducer from './foldersReducer';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    auth: authReducer,
    folderState: foldersReducer
});

export default rootReducer;