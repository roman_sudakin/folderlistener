import axios from 'axios';
export const startSpying =  (folderPath)=>{
    return (dispatch, getState) => {
        return axios.get('https://webhook.site/b8023e12-ce8e-49c8-a2e7-49dc9956d07c')
        .then(
                response => dispatch({type: 'SPYING_STARTED', folderPath}),
                err => dispatch({type: 'SPYING_STARTED', folderPath })
            ).catch( e => {
                console.log(e);
                dispatch({type: 'SPYING_STARTED', folderPath})
            })
        
    }
}