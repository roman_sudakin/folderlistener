import React, { Component } from 'react';
import FolderActionList from './folderCom/FolderActionList';
import FolderActions from './folderCom/FolderActions';
import SearchFolder from './folderCom/search.component';
import {connect} from 'react-redux';

class Dashboard extends Component {
  render() {
    const {actionsList} = this.props;
    return (
      <div className="dashboard container">
        <h1>Spy School</h1>
        <SearchFolder/>
        <FolderActionList actionsList={actionsList}/>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    actionsList: state.folderState.actionLogs
  }
}
export default connect(mapStateToProps)(Dashboard);
