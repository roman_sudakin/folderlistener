import React, { Component } from 'react'
import FolderActions from './FolderActions';

import {connect} from 'react-redux';
import { startSpying } from '../../../store/actions/folderActions';

class SearchFolder extends Component {
  state = {
      started: false,
      stoped: true,
      folderPath: ''
  }
  get PAthValue(){
      return this.state.folderPath;
  }
  _startSpying = (path) => {
      this.setState({folderPath: path})
      this.props.startSpying(path)
  }
 
  render() {
    return (
      <div className="search container">
        <FolderActions triggerSpy={this._startSpying}/>
      </div>
    )
  }
}
const mapStateToProps = ( dispatch ) => {
  return {
    startSpying: ( spyFolderPath ) => dispatch( startSpying( spyFolderPath ) )
  }
}
export default connect( null, mapStateToProps )( SearchFolder );
