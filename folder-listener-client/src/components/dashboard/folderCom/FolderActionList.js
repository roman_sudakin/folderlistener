import React from 'react'

const FolderActionList = ({actionsList}) => {
  return (
    <div className="folderList container">
      <ul className="collection with-header">
          <li className="collection-header"><h4>Folder Action Log</h4></li>
          {
            actionsList
                    .map( (action, i) => 
                        (<li key={i} className="collection-item">{action.desc}</li>))
          }
      </ul>
    </div>
  )
}

export default FolderActionList;
