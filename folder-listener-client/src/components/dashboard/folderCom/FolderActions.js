import React, { Component } from 'react';

class FolderActions extends Component {
  state = {
    pathValue: ''
  }
  handleChange = (e) => {
    e.preventDefault();
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  triggerSpy = (e) =>{
    e.preventDefault();
    if(this.state.pathValue){
      this.props.triggerSpy(this.state.pathValue)
    }
  }

  render() {
    return (
      <div className="folderButtons container">
        <div className="row">
          <form className="col s12">
            <div className="row">
              <div className="input-field col s12">
                <label htmlFor="pathValue">Path</label>
                <input placeholder="Enter Path to listen" 
                       id="pathValue" 
                       type="text"
                       onChange={this.handleChange}
                       />
              </div>
            </div>
          </form>
        </div>
        <button className="waves-effect waves-light btn"
                 onClick={this.triggerSpy}>
                <i className="material-icons right">directions_run</i> 
                start
        </button>
        <button className="waves-effect waves-red btn"><i className="material-icons right">pan_tool</i>stop</button>
        <button className="waves-effect waves-teal btn"><i className="material-icons right">info</i>status</button>
      </div>
    )
  }
}

export default FolderActions