import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
// import logo from './logo.svg';
import './App.css';
import NavBar from './components/layout/NavBar';
import Dashboard from './components/dashboard/DashBoard';
import SignIn from './components/auth/LogIn';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
        <NavBar/>
          <Switch>
            <Route exact path='/' component={Dashboard}/>
            <Route path='/login' component={SignIn}/>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
